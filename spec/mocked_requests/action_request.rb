def mock_action_request_with_url(action, options)
  response = ''
  url = [ Imaginary.configuration.server,
      action,
      '?',
      options.to_query_params].join
  stub_request(:get, url).
    to_return(status: 200, body: response, headers: {
        content_type: "image/png"
      })
end

def mock_action_request_with_file(action, request_options)
  response = ''
  request_options = request_options.select { |k,v| k != :file }
  url = [ Imaginary.configuration.server,
      action,
      '?',
      request_options.to_query_params].join
  stub_request(:post, url)
  .to_return(status: 200, body: response, headers: {
        content_type: "image/png"
      })
end
