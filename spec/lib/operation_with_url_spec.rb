RSpec.describe Imaginary do
  context 'Imaginary Sample Operation with URL' do
    let(:options) do
      {
        url: 'https://test/image.png',
        width: 200
      }
    end

    it 'resize the picture successfully' do
      mock_action_request_with_url('resize', options)
      result = Imaginary::Resize.new(options).result
      expect(result.headers[:content_type]).to eq 'image/png'
    end
  end
end
