RSpec.describe Imaginary do
  context 'Imaginary Sample Operation with File' do
    let(:options) do
      {
        file: File.dirname(__FILE__) + "/../sample/docker.png",
        width: 200
      }
    end

    it 'resize the picture successfully' do
      mock_action_request_with_file('resize', options)
      result = Imaginary::Resize.new(options).result
      expect(result.headers[:content_type]).to eq 'image/png'
    end
  end
end
