# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'ruby_imaginary'
  s.version     = '0.0.2'
  s.date        = '2021-03-21'
  s.summary     = 'Ruby client for imaginary service'
  s.description = 'Ruby client for imaginary service'
  s.authors     = ['Amirhosein Zolfaghari']
  s.email       = 'amirhosein.zlf@gmail.com'
  s.files       = Dir['lib/**/*.rb']
  s.homepage    = 'https://gitlab.com/azolf/imaginary-ruby'
  s.license       = 'MIT'
  s.require_paths = ['lib']
  s.add_runtime_dependency 'rest-client', '~> 2.0.2', '>= 2.0.2'
  s.required_ruby_version = '~> 2.4'
  s.extra_rdoc_files = ['README.md']
end
