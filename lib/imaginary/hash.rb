# frozen_string_literal: true

class Hash
  def to_query_params
    map do |k, v|
      "#{k}=#{v}"
    end.join('&')
  end
end
