# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class Blur < Operation
    exists? :sigma
  end
end
