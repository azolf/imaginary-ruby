# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class Extract < Operation
    exists? :top, and: :areawidth
  end
end
