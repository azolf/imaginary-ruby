# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class Crop < Operation
    exists? :wdith, or: :height
  end
end
