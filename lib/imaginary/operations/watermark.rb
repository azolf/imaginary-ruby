# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class Watermark < Operation
    exists? :text
  end
end
