# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class Convert < Operation
    exists? :type
  end
end
