# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class Flop < Operation
  end
end
