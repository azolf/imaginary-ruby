# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class Flip < Operation
  end
end
