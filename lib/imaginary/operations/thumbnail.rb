# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class Thumbnail < Operation
    exists? :width, and: :height
  end
end
