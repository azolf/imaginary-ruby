# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class WatermarkImage < Operation
    exists? :image
  end
end
