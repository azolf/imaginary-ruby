# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class Zoom < Operation
    exists? :factor
  end
end
