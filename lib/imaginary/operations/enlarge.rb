# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class Enlarge < Operation
    exists? :wdith, and: :height
  end
end
