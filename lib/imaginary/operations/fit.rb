# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class Fit < Operation
    exists? :width, and: :height
  end
end
