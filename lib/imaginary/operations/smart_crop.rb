# frozen_string_literal: true

# Imaginary module extended for crop support
module Imaginary
  class SmartCrop < Operation
    exists? :width, or: :height
  end
end
