# frozen_string_literal: true

# Imaginary module
module Imaginary
  class << self
    attr_accessor :configuration
  end

  def self.configure
    self.configuration ||= Configuration.new
    yield(configuration)
  end

  # configure class
  class Configuration
    attr_accessor :server
  end
end
