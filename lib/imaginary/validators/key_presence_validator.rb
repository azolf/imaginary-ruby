# frozen_string_literal: true

module Imaginary
  module Validators
    module KeyPresenceValidator
      def exists?(key, options = {})
        validator_options = []
        options.keys.each do |type|
          validator_options << Option.new(type, options[type])
        end
        validators << KeyValidator.new(key, validator_options)
      end

      def validators
        @validators ||= []
      end
    end
  end
end
