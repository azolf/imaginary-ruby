# frozen_string_literal: true

module Imaginary
  module Validators
    class Option
      attr_accessor :type
      attr_accessor :key

      def initialize(type, key)
        raise 'Invalid Validator Option Type' unless %i[and or].include? type

        self.type = type
        self.key = key
      end

      def append(result, object)
        return (result && object.include?(key)) if type == :and

        result || object.include?(key)
      end
    end
  end
end
