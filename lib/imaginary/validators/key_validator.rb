# frozen_string_literal: true

module Imaginary
  module Validators
    class KeyValidator
      attr_accessor :options
      attr_accessor :key

      def initialize(key, options)
        self.key = key
        self.options = options
      end

      def valid?(object)
        validation_resut = object.include?(key)
        options.each do |option|
          validation_resut = option.append(validation_resut, object)
        end

        validation_resut
      end
    end
  end
end
