# frozen_string_literal: true

module Imaginary
  class InvalidOption < StandardError
  end
end
