# frozen_string_literal: true

module Imaginary
  class Operation
    attr_accessor :options
    attr_accessor :file_input_address

    include Imaginary::RequestHandler
    extend Imaginary::Validators::KeyPresenceValidator

    def file_input?
      !file_input_address.nil?
    end

    def initialize(options)
      self.file_input_address = options[:file]
      options.delete(:file)
      self.options = options
    end

    def valid?
      self.class.validators.each do |v|
        return false unless v.valid?(options)
      end

      true
    end

    def remote_action
      self.class.name.split('::').last.downcase
    end

    def url
      [
        Imaginary.configuration.server,
        remote_action,
        '?',
        options.to_query_params
      ].join
    end
  end
end
