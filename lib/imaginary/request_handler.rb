# frozen_string_literal: true

module Imaginary
  module RequestHandler
    def result
      raise InvalidOption unless valid?
      return execute_with_file if file_input?

      execute_with_url
    end

    private

    def execute_with_file
      RestClient.post url, file: File.new(file_input_address, 'rb')
    end

    def execute_with_url
      RestClient.get url
    end
  end
end
