# frozen_string_literal: true

require_relative 'imaginary/validators/key_validator.rb'
require_relative 'imaginary/validators/option.rb'
require_relative 'imaginary/validators/key_presence_validator.rb'

require_relative 'imaginary/configuration.rb'
require_relative 'imaginary/request_handler.rb'
require_relative 'imaginary/operation.rb'

require_relative 'imaginary/operations/blur.rb'
require_relative 'imaginary/operations/convert.rb'
require_relative 'imaginary/operations/crop.rb'
require_relative 'imaginary/operations/enlarge.rb'
require_relative 'imaginary/operations/extract.rb'
require_relative 'imaginary/operations/fit.rb'
require_relative 'imaginary/operations/flip.rb'
require_relative 'imaginary/operations/flop.rb'
require_relative 'imaginary/operations/resize.rb'
require_relative 'imaginary/operations/rotate.rb'
require_relative 'imaginary/operations/smart_crop.rb'
require_relative 'imaginary/operations/thumbnail.rb'
require_relative 'imaginary/operations/zoom.rb'
require_relative 'imaginary/operations/watermark.rb'
require_relative 'imaginary/operations/watermark_image.rb'

require_relative 'imaginary/hash.rb'

require_relative 'imaginary/exceptions/invalid_option.rb'

require 'rest-client'

module Imaginary
end
