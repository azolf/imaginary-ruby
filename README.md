# Imaginary Ruby
Minimalist ruby gem which is programmatic stream capable interface for [imaginary](https://github.com/h2non/imaginary) server.

Supports multiple image operations such as resize, crop, zoom, watermark, rotate... and both local and remote URL based image source processing.

## Installation

`gem install ruby_imaginary`

or add the line below to your Gemfile

`gem 'ruby_imaginary'`

## Configurations

You need to set the imaginary server address like this.
```
Imaginary.configure do |config|
  # You could set it directly or getting it from ENV variables
  config.server = 'http://localhost:9000/'
end
```

Also for rails application you could create an initializer at `config/initializers/imaginary.rb`

## Operations

- Resize
- Enlarge
- Crop
- SmartCrop (based on libvips built-in algorithm)
- Rotate (with auto-rotate based on EXIF orientation)
- Flip (with auto-flip based on EXIF metadata)
- Flop
- Zoom
- Thumbnail
- Fit
- Blur
- Watermark (customizable by text)
- WatermarkImage

All the options are as same as the imaginary server documentation.

## How to Use

Each of the operation above has a class under `Imaginary` module which accepts a hash for initializing.

```
options = {
  file: './images/1.jpg',
  text: 'MyBrand',
}
operation = Imaginary::Watermark.new(options)

result = operation.result

File.write('result.jpg', result)
```

Also before running the operation you could user the `valid?` method to make sure all the options are correct for this operation and avoid any failures.

```
operation.valid?
```
